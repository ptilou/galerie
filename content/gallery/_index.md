---
title: "Images"
date: 2018-07-05T10:27:36+02:00
draft: false
---
# Fond d'écran
Voici les fond d'écran de Fedora.

*Pourquoi prendre les fonds d'écran de Fedora ?*

Je ne savais pas quoi prendre comme image, alors je me suis dit : *il doit bien existé des photos sympas sur mon ordinateur* (autre que des photos personnelles) et alors que je réflichissais (oui, ça m'arrive, de temps en temps) sur mon bureau et là, j'ai eu l'illumination ! Pourquoi ne pas prendre les fonds d'écran de ma distribution !!
